const resultDiv = document.querySelector("#divs");
const buttonDiv = document.querySelector("#button-div");

function taoThe() {
    var content = [];
    for (var i = 1; i <= 10; i++) {
        if (i % 2 != 0) {
            content.push('<div class="odd-div">Div lẻ</div>');
        } else {
            content.push('<div class="even-div">Div chẳn</div>');
        }
    }
    resultDiv.innerHTML = content.join("");
}

buttonDiv.addEventListener("Tao The", taoThe);